#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""
import sys

from setuptools import setup, find_packages
from itertools import chain

with open('README.rst') as readme_file:
    readme = readme_file.read()


setup(
    name='funcsubs',
    version='0.4.0',
    description="General function and method subscription system",
    long_description=readme,
    author="Bogdan Gladyshev",
    author_email='siredvin.dark@gmail.com',
    url='https://gitlab.com/SirEdvin/funcsubs',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'frozendict>=1.2'
    ],
    license="MIT license",
    zip_safe=False,
    keywords='subscribtion event',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    tests_require=[],
    setup_requires=[],
    extras_require={}
)
