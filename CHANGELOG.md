# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.4.0] - 2019-04-01

### Added

- `inplace` signals without exception hiding logic

## [0.3.1] - 2019-03-29

### Fixed

- Empty election

## [0.3.0] - 2019-03-29

### Added

- election hook