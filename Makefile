lint:
	pylint funcsubs tests
	pycodestyle funcsubs tests
	mypy funcsubs --ignore-missing-imports
inspect:
	pipenv check
	bandit -r funcsubs       
docs:
	sphinx-build -b html ./source ./build
pytest:
	pytest --duration 20 --cov funcsubs --cov-report term-missing tests